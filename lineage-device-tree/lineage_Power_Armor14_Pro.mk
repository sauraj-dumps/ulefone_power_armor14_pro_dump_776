#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Power_Armor14_Pro device
$(call inherit-product, device/ulefone/Power_Armor14_Pro/device.mk)

PRODUCT_DEVICE := Power_Armor14_Pro
PRODUCT_NAME := lineage_Power_Armor14_Pro
PRODUCT_BRAND := Ulefone
PRODUCT_MODEL := Power Armor14 Pro
PRODUCT_MANUFACTURER := ulefone

PRODUCT_GMS_CLIENTID_BASE := android-gotron

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Power_Armor14_Pro-user 12 SP1A.210812.016 1647505568 release-keys"

BUILD_FINGERPRINT := Ulefone/Power_Armor14_Pro_EEA/Power_Armor14_Pro:12/SP1A.210812.016/1647505568:user/release-keys

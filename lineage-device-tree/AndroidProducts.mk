#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Power_Armor14_Pro.mk

COMMON_LUNCH_CHOICES := \
    lineage_Power_Armor14_Pro-user \
    lineage_Power_Armor14_Pro-userdebug \
    lineage_Power_Armor14_Pro-eng
